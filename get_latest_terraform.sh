#!/bin/bash

tmpfile=`mktemp`

wget -q -O $tmpfile --convert-links https://releases.hashicorp.com/terraform/
release_url=`xmllint --html --xpath '//ul/li[2]/a/@href' $tmpfile 2>/dev/null | sed 's/ href="\([^"]*\)"/\1\n/g'`
tmpfile2=`mktemp`
wget -q -O $tmpfile2 --convert-links $release_url
tf_url=`xmllint --html --xpath '//ul/li/a[@data-os="linux" and @data-arch="amd64"]/@href' $tmpfile2 2>/dev/null | sed 's/ href="\([^"]*\)"/\1\n/g'`
ziptemp=`mktemp`
wget $tf_url -O $ziptemp
unzip -d /usr/local/bin/ $ziptemp

rm $tmpfile $tmpfile2 $ziptemp
