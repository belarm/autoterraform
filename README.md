# autoterraform

Attempts to build terraform code from existing cloud infrastructure.

NB: I recommend running these commands from an empty folder!

## Usage

In the module folder, a python script called test.py can be found.
When run, it will generate valid terraform code from the output of several azure CLI commands, including:

* `az ad user list`
* `az ad group list`
* `az ad app list`
* `az ad sp list`
* `az storage account list`
* `az network nsg list`

The output of this command will be several files: one for each resource type, and a
shell script named 'import.sh' which, when run, will import the discovered
resources into terraform's state.

After running both test.py and import.sh, your azure AD resources will be fully
managed by terraform.

## One step

This can all be done in a single step by running the `full_run.sh` script in the module directory

## Docker-compose
A docker image and docker-compose file are also included which will run this process
in an environment containing all the needed software and libraries. Simply run:

    ./run_in_docker.sh

from the top-level directory and get some coffee. Your terraform config will
land in the `tf` subfolder. Note that these files will be owned by root - to resolve this, run `sudo chown -R <your username> tf`
