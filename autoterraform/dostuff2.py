#!/usr/bin/env python3

import os
import json
import jmespath
from time import sleep
import string
import subprocess
import azure
import re
from azure.common.client_factory import get_client_from_cli_profile
from azure.mgmt.resource import ResourceManagementClient


resource_to_tfname = {
    "Microsoft.AAD/DomainServices": None,
    "Microsoft.Automation/automationAccounts": "azurerm_automation_account",
    "Microsoft.CertificateRegistration/certificateOrders": None,
    "Microsoft.ClassicStorage/storageAccounts": None,
    "Microsoft.Compute/disks": "azurerm_managed_disk",
    "Microsoft.Compute/restorePointCollections": None,
    "Microsoft.Compute/virtualMachines": "azurerm_virtual_machine",
    "Microsoft.Compute/virtualMachines/extensions": "azurerm_virtual_machine_extension",
    "Microsoft.DBforMySQL/servers": "azurerm_mysql_server",
    # "Microsoft.EventGrid/topics": "azurerm_eventgrid_topic",
    "microsoft.insights/actiongroups": "azurerm_monitor_action_group",
    # "microsoft.insights/alertrules": "azurerm_metric_alertrule",
    "microsoft.insights/autoscalesettings": "azurerm_autoscale_setting",
    # Sensitive value - come back?
    "microsoft.insights/components": "azurerm_application_insights",
    # "microsoft.insights/metricalerts": "azurerm_monitor_metric_alert",
    # "Microsoft.KeyVault/vaults": "azurerm_key_vault",
    "Microsoft.Logic/integrationAccounts": None,
    # "Microsoft.Logic/workflows": "azurerm_logic_app_workflow",
    "Microsoft.MarketplaceApps/classicDevServices": None,
    # "Microsoft.Network/dnszones": "azurestack_dns_zone",
    "Microsoft.Network/loadBalancers": "azurerm_lb",
    "Microsoft.Network/networkInterfaces": "azurerm_network_interface",
    "Microsoft.Network/networkSecurityGroups": "azurerm_network_security_group",
    "Microsoft.Network/networkWatchers": "azurerm_network_watcher",
    "Microsoft.Network/publicIPAddresses": "azurerm_public_ip",
    # "Microsoft.Network/virtualNetworks": "azurerm_virtual_network",
    "Microsoft.OperationalInsights/workspaces": "azurerm_log_analytics_workspace",
    "Microsoft.OperationsManagement/solutions": "azurerm_log_analytics_solution",
    "Microsoft.Portal/dashboards": None,
    "Microsoft.RecoveryServices/vaults": "azurerm_recovery_services_vault",
    "Microsoft.Scheduler/jobcollections": "azurerm_scheduler_job_collection",
    "Microsoft.ServiceBus/namespaces": "azurerm_servicebus_namespace",
    "Microsoft.Sql/servers": "azurerm_sql_server",
    "Microsoft.Sql/servers/databases": "azurerm_sql_database",
    "Microsoft.SqlVirtualMachine/SqlVirtualMachines": None,
    # Sensitive value - come back?
    "Microsoft.Storage/storageAccounts": "azurerm_storage_account",
    "Microsoft.VisualStudio/account": None,
    "microsoft.visualstudio/account/project": None,
    "Microsoft.Web/certificates": None,
    # "Microsoft.Web/connections": None,
    "Microsoft.Web/serverFarms": None,
    "Microsoft.Web/sites": "azurerm_app_service",
    "Sendgrid.Email/accounts": None
}

try:
    with open('state.json','r') as infile:
        state = json.load(infile)
        existing_resources = jmespath.search("resources[].instances[].attributes.id", state)
except:
    existing_resources = []
print(f"Found {len(existing_resources)} resources already in state")
THIS_DIR = os.path.dirname(os.path.abspath(__file__))
try:
    with open(os.path.join(THIS_DIR, 'static_data', 'schema.json'),'r') as schema_file:
        schema = json.load(schema_file)
except FileNotFoundError:
    raise FileNotFoundError("Please run get_schemas.py")

ansi_escape = re.compile(r'\x1B[@-_][0-?]*[ -/]*[@-~]')

tr_table = str.maketrans('. /(', '____')

def makenames(type, name):
    snake_name = name.translate(tr_table).replace(')','')
    type = resource_to_tfname[type]
    if snake_name[0] in string.digits:
        snake_name = 'a_' + snake_name
    return type, snake_name, f'{type}.{snake_name}'

# from azure.mgmt.compute import ComputeManagementClient
# client = get_client_from_cli_profile(ComputeManagementClient)
rc = get_client_from_cli_profile(ResourceManagementClient)
rc.resources.list()
resources = list(rc.resources.list())
to_tf = {}
count = 0
for resource in resources:
    if resource.type in resource_to_tfname and resource_to_tfname[resource.type] is not None:
        type, snake_name, full_name = makenames(resource.type, resource.name)
        tf_declr = f'resource "{type}" "{snake_name}" {{}}'
        if resource.id in existing_resources:
            print(f"Skipping {full_name} (id: {resource.id} already in state)")
            continue
        print(f"Will generate TF config for {full_name}")
        if resource.type not in to_tf:
            to_tf[resource.type] = []
        to_tf[resource.type].append(
            (tf_declr, resource)
        )
        count += 1
print(f"Planning to import {count} resources...")
# sleep(10)
# Now we have a dictionary of lists of resources, sorted by type
import_commands = []
get_state_commands = []
output_filenames = []
to_delete = []
to_rename = []
for type, resources in to_tf.items():
    # Make two files - one to hold the skeleton definition to enable import,
    # the other to hold the finalized TF definition
    fname = f'{resource_to_tfname[type]}.tf.tmp'
    temp_fname = f'_temp_{resource_to_tfname[type]}.tf'
    with open(temp_fname,'w') as outfile:
        for declr, resource in resources:
            outfile.write(declr + '\n')
            type, snake_name, full_name = makenames(resource.type, resource.name)

            snake_name = resource.name.translate(tr_table)
            if snake_name[0] in string.digits:
                snake_name = 'a_' + snake_name
            import_commands.append(["terraform", "import", f"{full_name}", f"{resource.id}"])
            get_state_commands.append([["terraform", "state", "show", f"{full_name}"],["sed", "s/\x1b\[[0-9;]*[a-zA-Z]//g"], fname])
            # output_filenames.append(fname)
    to_delete.append(temp_fname)
    to_rename.append(fname)

def depthmod(s):
    '''Given a string, return an integer indicating how much it
    increases/decreases the 'depth'. [ and { increase the depth, } and ]
    decrease it.'''
    return s.count('[') + \
        s.count('{') + \
        s.count('(') - \
        s.count(')') - \
        s.count('}') - \
        s.count(']')

def iterate_attributes(proc):
    depth = 0
    lines = []
    for _line in proc.stdout:
        line = _line.decode('utf-8')
        mod = depthmod(line)
        depth += mod
        if line.strip():
            lines.append(line.replace('(sensitive value)', '"(sensitive value)"'))
        if depth == 0 and lines:
            yield lines
            lines=[]
    if lines and lines != ['}\n']:
        yield lines

for cmd in import_commands:
    print(f"Running {cmd}")
    proc0 = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    proc0.wait()

for (cmd0, cmd1, fname) in get_state_commands:
    proc0 = subprocess.Popen(cmd0, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    proc1 = subprocess.Popen(cmd1, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=proc0.stdout)
    with open(fname,'a') as outfile:
        comment = proc1.stdout.readline()
        outfile.write(comment.decode('utf-8'))
        resource_definition = proc1.stdout.readline().decode('utf-8')
        outfile.write(resource_definition)
        resource_type = resource_definition.split()[1][1:-1]
        for attribute in iterate_attributes(proc1):
            # Now, we should filter out invalid attributes (those that are not arguments)
            aname = attribute[0].split(maxsplit=1)[0]
            if aname in schema[resource_type]:
                outfile.write(''.join(attribute))
            else:
                outfile.write(''.join(["# " + line for line in attribute]))
        outfile.write("}\n# --------------------------")
        print(f"Rendered terraform code for {resource_definition[:-2]}")
# for cmd in to_run:
#     print(cmd)
#     os.system(cmd)
print("Deleteing temporary files...")
for file in to_delete:
    os.remove(file)
print("Moving new tf files into place...")
for file in to_rename:
    newname = file[:-4]
    os.rename(file, newname)
