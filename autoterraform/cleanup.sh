#!/bin/bash
rm -fv _temp_*.tf
rm -fv azurerm_*.tf
rm -fv terraform.tfstate.backup
rm -fv terraform.tfstate
