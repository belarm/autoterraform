#!/usr/bin/env python3
import os
import json
import subprocess

# This submodule handles generating, caching, and loading large configuration values
# such as schemas



proc = subprocess.Popen(["terraform","providers","schema","-json"],stdout=subprocess.PIPE)
# proc.wait()
schema = json.load(proc.stdout)
# print("Got schema!")

bare_schema = {}
os.makedirs('templates',exist_ok=True)
# resources = schema['provider_schemas']['azurerm']['resource_schemas']
for provider in schema['provider_schemas'].values():
    # print(provider)
    for name, resource in provider['resource_schemas'].items():
#        print(f"Adding {name}")
#        with open(f'templates/{name}.tf.j2','w') as templatefile:
#            templatefile.write(f'resource "{name}" "{{{{tf_name}}}}" {{\n')
#            for key, val in resource['block']['attributes'].items():
#                templatefile.write(f"    {key} = {val['
#        bare_schema[name] = []
            for key, val in resource['block']['attributes'].items():
                if 'computed' in val and val['computed'] == True:
                    leadchar = '#'
                else:
                    leadchar = ' '
                line = f'{leadchar}    {key:30} = {val["type"]}\n'
                templatefile.write(line)
            templatefile.write("}\n")
#                bare_schema[name].append(key)
#        if 'block_types' in resource['block']:
#            for key, val in resource['block']['block_types'].items():
#                # if 'computed' not in val or val['computed'] != True:
#                bare_schema[name].append(key)
with open('schema.json','w') as outfile:
    json.dump(bare_schema, outfile, indent=3)
#    # print(attributes.keys())
## print(resources.keys())
