#!/usr/bin/env python3

import os
import json
import jmespath
from time import sleep
import string
import subprocess
import azure
# import re
from azure.common.client_factory import get_client_from_cli_profile
from azure.mgmt.resource import ResourceManagementClient

THIS_DIR=os.path.abspath(os.path.dirname(__file__))
resource_to_tfname = json.load(open(os.path.join(THIS_DIR, 'static_data/resource_mappings_orig.json')))

try:
    with open('terraform.tfstate','r') as infile:
    # with open('state.json','r') as infile:
        state = json.load(infile)
        # existing_resources = jmespath.search("resources[].join('.', [type, name])", state)
        existing_resources = jmespath.search("resources[].instances[].attributes.id", state)
except:
    existing_resources = []
print(f"Found {len(existing_resources)} resources already in state")

try:
    with open(os.path.join(THIS_DIR, 'static_data/schema.json'),'r') as schema_file:
        schema = json.load(schema_file)
except FileNotFoundError:
    raise FileNotFoundError("Please run get_schemas.py")

# ansi_escape = re.compile(r'\x1B[@-_][0-?]*[ -/]*[@-~]')

# from string import maketrans
tr_table = str.maketrans('. /(', '____')

def makenames(type, name):
    snake_name = name.translate(tr_table).replace(')','')
    type = resource_to_tfname[type]
    if snake_name[0] in string.digits:
        snake_name = 'a_' + snake_name
    return type, snake_name, f'{type}.{snake_name}'

# from azure.mgmt.compute import ComputeManagementClient
# client = get_client_from_cli_profile(ComputeManagementClient)
rc = get_client_from_cli_profile(ResourceManagementClient)
rc.resources.list()
resources = list(rc.resources.list())
to_tf = {}
count = 0
skipped_resources = []
for resource in resources:
    if resource.type in resource_to_tfname and resource_to_tfname[resource.type] is not "":
        type, snake_name, full_name = makenames(resource.type, resource.name)
        tf_declr = f'resource "{type}" "{snake_name}" {{}}'
        if resource.id in existing_resources:
            print(f"Skipping {full_name} (id: {resource.id} already in state)")
            continue
        print(f"Will generate TF config for {full_name}")
        if resource.type not in to_tf:
            to_tf[resource.type] = []
        to_tf[resource.type].append(
            (tf_declr, resource)
        )
        count += 1
    else:
        print(f"Can't handle resources of type {resource.type}; skipping {resource.id}")
        skipped_resources.append(resource.as_dict())
print(f"Planning to import {count} resources...")
# sleep(10)
# Now we have a dictionary of lists of resources, sorted by type
import_commands = []
get_state_commands = []
output_filenames = []
to_delete = []
to_rename = []
for type, resources in to_tf.items():
    # Make two files - one to hold the skeleton definition to enable import,
    # the other to hold the finalized TF definition
    fname = f'{resource_to_tfname[type]}.tf.tmp'
    temp_fname = f'_temp_{resource_to_tfname[type]}.tf'
    with open(temp_fname,'w') as outfile:
        for declr, resource in resources:
            outfile.write(declr + '\n')
            type, snake_name, full_name = makenames(resource.type, resource.name)

            snake_name = resource.name.translate(tr_table)
            if snake_name[0] in string.digits:
                snake_name = 'a_' + snake_name
            import_commands.append(["terraform", "import", f"{full_name}", f"{resource.id}"])
            get_state_commands.append([["terraform", "state", "show", f"{full_name}"],["sed", "s/\x1b\[[0-9;]*[a-zA-Z]//g"], fname])
            # output_filenames.append(fname)
    to_delete.append(temp_fname)
    to_rename.append(fname)

def depthmod(s):
    '''Given a string, return an integer indicating how much it
    increases/decreases the 'depth'. [ and { increase the depth, } and ]
    decrease it.'''
    return s.count('[') + \
        s.count('{') + \
        s.count('(') - \
        s.count(')') - \
        s.count('}') - \
        s.count(']')

# def fetchline(handle, depth=0):
#     '''Given a file handle, returns one line and the corresponding depthmod'''
#     line = ''
#     # Skip blank lines
#     while(line == ''):
#         line = handle.readline().decode('utf-8').strip()
#     mod = depthmod(line)
#     return line, depth + mod

def iterate_attributes(proc):
    depth = 0
    lines = []
    for _line in proc.stdout:
        line = _line.decode('utf-8')
        # print(line)
        mod = depthmod(line)
        depth += mod
        # print(depth, mod)
        if line.strip():
            lines.append(line)
        if depth == 0 and lines:
            yield lines
            # yield ''.join(lines)
            lines=[]
    if lines and lines != ['}\n']:
        yield lines
        # yield ''.join(lines)


# def iterate_attributes(proc):
#     # THIS IS TERRIBLE
#     depth = 0
#     file_handle = proc.stdout
#     proc.wait()
#     line, depth = fetchline(file_handle, depth)
#     # line = file_handle.readline().decode('utf-8')
#     while(line != '}\n'):
#         if line.startswith('    ') and line[4] in string.ascii_letters:
#             attr_lines = [line]
#             line = file_handle.readline().decode('utf-8')
#             while(
#                     not line.startswith('}')
#                     and line[0] != '\n'
#                     and line[5] == ' '
#                 ):
#                 attr_lines.append(line)
#                 line = file_handle.readline().decode('utf-8')
#             if (len(line) >= 5 and line[4] == '}'):
#                 attr_lines.append(line)
#             yield ''.join(attr_lines)
#         else:
#             line = file_handle.readline().decode('utf-8')

for cmd in import_commands:
    print(f"Running {' '.join(cmd)}")
    subprocess.call(cmd)
    # proc0 = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    # proc0.wait()
    # proc0.communicate()

print("Resource imported. Making terraform")

for (cmd0, cmd1, fname) in get_state_commands:
    print(f"---Running cmd: {' '.join(cmd0)}")
    proc0 = subprocess.Popen(cmd0, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    proc1 = subprocess.Popen(cmd1, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=proc0.stdout)
    with open(fname,'a') as outfile:
        comment = proc1.stdout.readline()
        outfile.write(comment.decode('utf-8'))
        resource_definition = proc1.stdout.readline().decode('utf-8')
        outfile.write(resource_definition)
        print(resource_definition)
        resource_type = resource_definition.split()[1][1:-1]
        # print("Got definition:")
        # print(resource_definition)
        # print(resource_type)
        # print(schema[resource_type])
        for attribute in iterate_attributes(proc1):
            # Now, we should filter out invalid attributes (those that are not arguments)
            # print(attribute)
            aname = attribute[0].split(maxsplit=1)[0]
            # if aname not in ['id']:
            # print(aname)
            # This should probably be a function
            filtered_attribute = [attrib.replace('(sensitive value)', '"##############"') for attrib in attribute]
            # attribute[0] = attribute[].replace('(sensitive value)', '"##############"')
            if aname in schema[resource_type]:
                outfile.write(''.join(filtered_attribute))
            else:
                outfile.write(''.join(["# " + line for line in filtered_attribute]))
                # print("This is a bad one")
        outfile.write("}\n# --------------------------")
        print(f"Rendered terraform code for {resource_definition[:-2]}")
# for cmd in to_run:
#     print(cmd)
#     os.system(cmd)
print("Deleteing temporary files...")
for file in to_delete:
    os.remove(file)
print("Moving new tf files into place...")
for file in to_rename:
    newname = file[:-4]
    os.rename(file, newname)
json.dump(skipped_resources, open('skipped_resources.json', 'w'), indent=3)
