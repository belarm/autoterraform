#!/usr/bin/env python3
import os
import jinja2
import logging
import json
import subprocess


# Gather some path variables for creating our jinja2 environment
global_j2_env = None
global_az_rest_session = None
THIS_DIR = os.path.dirname(os.path.abspath(__file__))

# Set up basic logging
LOG_LEVEL = os.environ.get('LOGLEVEL', 'WARNING').upper()
logging.basicConfig(level=LOG_LEVEL)
# tr_table = str.maketrans('. /(@\'', '______')


# TODO: separate value fixing from id derivation
def fix_val(val):
    # name = val['displayName'].translate(tr_table).replace(')','')
    try:
        id = val['objectId']
    except KeyError:
        id = val['name']
    id_name = 'id_' + id.replace('-','_')
    newval = dict(val)
    newval['tf_name'] = id_name
    for k, v in val.items():
        if v == True:
            newval[k] = 'true'
        if v == False:
            newval[k] = 'false'
    return newval, id, id_name


class Terraformer(object):
    def __init__(self, j2_env=None, rest_session=None):
        global global_j2_env
        global global_az_rest_session

        if j2_env is None:
            if global_j2_env is None:
                logging.debug("Creating global jinja2 environment")
                global_j2_env = jinja2.Environment(
                    loader=jinja2.FileSystemLoader(
                        os.path.join(THIS_DIR, 'templates')
                ))
                for template in global_j2_env.list_templates():
                    logging.debug(f"Got template: {template}")
            self.j2_env = global_j2_env
        else:
            self.j2_env = j2_env

        if rest_session is None:
            if global_az_rest_session is None:
                logging.debug("Creating global Azure REST session")
                from azure.common.credentials import get_azure_cli_credentials
                creds, subid = get_azure_cli_credentials()
                global_az_rest_session = creds.signed_session()
            self.rest_session = global_az_rest_session
        else:
            self.rest_session = rest_session

    def read_object(self):
        raise NotImplementedError

class AzureTerraformer(Terraformer):
    def __init__(self, *args, **kwargs):
        super().__init__(kwargs.get('j2_env', None))
        self.object_list = []
        self.cmd = ""

    def read_objects(self):
        self.object_list = json.loads(subprocess.check_output(self.cmd.split()))

    def render_templates(self):
        if not self.object_list:
            self.read_objects()
        with open(self.tf_filename,'a') as outfile, open('import.sh','a') as importfile:
            for val in self.object_list:
                newval, id, id_name = fix_val(val)
                if 'id' in newval:
                    id = newval['id']
                try:
                    # TODO: add option to skip this check?
                    subprocess.check_call(["terraform", "state", "list", f"{self.tf_class}.{id_name}"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
                except subprocess.CalledProcessError:
                    outfile.write(self.template.render(**newval))
                    outfile.write('\n')
                    importfile.write(f'terraform import {self.tf_class}.{id_name} "{id}"\n')
                    logging.info(f"Created HCL for {self.tf_class}.{id_name}")

                else:
                    logging.debug(f"Resource {self.tf_class}.{id_name} already in TF state")
        # for val in self.object_list:


class AzureADUsersTerraformer(AzureTerraformer):
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.cmd = "az ad user list"
        self.template = self.j2_env.get_template('azuread_user.tf.j2')
        self.tf_filename = "azure_ad_users.tf"
        self.tf_class = "azuread_user"

class AzureADApplicationsTerraformer(AzureTerraformer):
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.cmd = "az ad app list --all"
        self.template = self.j2_env.get_template('azuread_application.tf.j2')
        self.tf_filename = "azure_ad_applications.tf"
        self.tf_class = "azuread_application"

class AzureADServicePrincipalsTerraformer(AzureTerraformer):
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.cmd = "az ad sp list --all"
        self.template = self.j2_env.get_template('azuread_service_principal.tf.j2')
        self.tf_filename = "azure_ad_service_principals.tf"
        self.tf_class = "azuread_service_principal"

class AzureADGroupsTerraformer(AzureTerraformer):
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.cmd = "az ad group list"
        self.template = self.j2_env.get_template('azuread_group.tf.j2')
        self.tf_filename = "azure_ad_group.tf"
        self.tf_class = "azuread_group"

    def render_templates(self):
        if not self.object_list:
            self.read_objects()
        with open(self.tf_filename,'a') as outfile, open('import.sh','a') as importfile:
            for val in self.object_list:
                newval, id, id_name = fix_val(val)
                try:
                    subprocess.check_call(["terraform", "state", "list", f"{self.tf_class}.{id_name}"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
                except subprocess.CalledProcessError:
                    members = json.loads(
                        subprocess.check_output(
                            ['az', 'ad', 'group', 'member', 'list', '-g', id, '--query', '[*].objectId']
                        )
                    )
                    owners = json.loads(
                        subprocess.check_output(
                            ['az', 'ad', 'group', 'owner', 'list', '-g', id, '--query', '[*].objectId']
                        )
                    )
                    newval['members'] = members
                    newval['owners'] = owners
                    outfile.write(self.template.render(**newval))
                    outfile.write('\n')
                    importfile.write(f'terraform import {self.tf_class}.{id_name} "{id}"\n')
                    logging.info(f"Created HCL for {self.tf_class}.{id_name}")
                else:
                    logging.debug(f"Resource {self.tf_class}.{id_name} already in TF state")

class AzureStorageAccount(AzureTerraformer):
    default_network_ruleset = {
        "bypass": "AzureServices",
        "defaultAction": "Allow",
        "ipRules": [],
        "virtualNetworkRules": []
    }
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.cmd = "az storage account list"
        self.template = self.j2_env.get_template('resource_azurerm_storage_account.tf.j2')
        self.tf_filename = "azurerm_storage_account.tf"
        self.tf_class = "azurerm_storage_account"

    def render_templates(self):
        if not self.object_list:
            self.read_objects()
        with open(self.tf_filename,'a') as outfile, open('import.sh','a') as importfile:
            for val in self.object_list:
                newval, id, id_name = fix_val(val)
                try:
                    subprocess.check_call(["terraform", "state", "list", f"{self.tf_class}.{id_name}"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
                except subprocess.CalledProcessError:
                    atp_url = f"https://management.azure.com{newval['id']}/providers/Microsoft.Security/advancedThreatProtectionSettings/current?api-version=2017-08-01-preview"
                    newval['enable_advanced_threat_protection'] = json.loads(self.rest_session.get(atp_url).text)['properties']['isEnabled']
                    # TODO: Is this even right?
                    newval['account_replication_type'] = newval['sku']['name'].split('_')[-1]
                    # Don't specify the default ruleset, or TF will try to apply it
                    if newval['networkRuleSet'] == self.default_network_ruleset:
                        del newval['networkRuleSet']
                    outfile.write(self.template.render(**newval))
                    outfile.write('\n')
                    importfile.write(f'terraform import {self.tf_class}.{id_name} "{newval["id"]}"\n')
                    logging.info(f"Created HCL for {self.tf_class}.{id_name}")
                else:
                    logging.debug(f"Resource {self.tf_class}.{id_name} already in TF state")

class AzureNetworkSecurityGroup(AzureTerraformer):
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.cmd = "az network nsg list"
        self.template = self.j2_env.get_template('resource_azurerm_network_security_group.tf.j2')
        self.tf_filename = "azurerm_network_security_group.tf"
        self.tf_class = "azurerm_network_security_group"

class AmazonTerraformer(Terraformer):
    pass
