#!/bin/bash
# echo "Downloading any needed documentation..."
wget -c -r -np https://www.terraform.io/docs/providers/azurerm/ -q
# mkdir -p import_statements
# echo "extracting import statements..."
echo "{"
IFS=$' '
for i in www.terraform.io/docs/providers/azurerm/r/*.html
do
	if grep "terraform import" $i > /dev/null
	then
		bname=`basename $i`
		target=${bname/.html/.sh}
		import=$(cat $i | grep terraform\ import | cut -f4 -d\> |cut -f1 -d\<)
		if [[ $import != "terraform"* ]]
		then
			import=$(cat $i | grep terraform\ import | cut -f6 -d\> |cut -f1 -d\<)
		fi
		read _ _ tf_resource az_resource <<< $import
		# az_resource=`echo $az_resource`
		if [[ $notfirst ]]
		then
			echo ","
		else
			notfirst="true"
		fi
		echo -n "   \"${az_resource}\" : \"${tf_resource}\""
		# echo Added $tf_resource
		# echo $import > import_statements/${target}
	# else
		# echo "Could not find an import example in $i"
	fi
done
echo
echo "}"
# echo "Done. You will need to manually fix the following files before proceding:"
# wc import_statements/*|grep "^    .     0"|cut -f2 -d/ |cut -f1 -d.
