#!/usr/bin/env python3
import json
import subprocess
import os
proc = subprocess.Popen(["terraform","providers","schema","-json"],stdout=subprocess.PIPE)
# proc.wait()
schema = json.load(proc.stdout)
# print("Got schema!")

bare_schema = {}
os.makedirs('templates',exist_ok=True)
# resources = schema['provider_schemas']['azurerm']['resource_schemas']
for provider in schema['provider_schemas'].values():
    # print(provider)
    for name, resource in provider['resource_schemas'].items():
        print(f"Adding {name}")
        bare_schema[name] = []
        for key, val in resource['block']['attributes'].items():
            if 'computed' not in val or val['computed'] != True:
                bare_schema[name].append(key)
        if 'block_types' in resource['block']:
            for key, val in resource['block']['block_types'].items():
                if 'computed' not in val or val['computed'] != True:
                    bare_schema[name].append(key)
with open('static_data/schema.json','w') as outfile:
   json.dump(bare_schema, outfile, indent=3)
#    # print(attributes.keys())
## print(resources.keys())
