#!/usr/bin/env python3

import json
import jmespath

with open('terraform.tfstate','r') as inf:
    d = json.load(inf)
ids = set(jmespath.search('resources[].instances[].attributes.id', d))
with open('dustin.json','r') as inf:
    d2 = json.load(inf)
ids2 = set(jmespath.search('values.root_module.resources[].values.id', d2))
duplicates = ids.intersection(ids2)
for dup in duplicates:
    print(dup)
print(len(ids))
