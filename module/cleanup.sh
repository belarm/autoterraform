#!/bin/bash
rm -fv apps.tf users.tf groups.tf service_principals.tf
rm -rf .terraform
rm -fv terraform.tfstate.backup
rm -fv terraform.tfstate
rm -fv import.sh
