#!/usr/bin/env python3

import os
import json
import subprocess
from jinja2 import Environment, FileSystemLoader

resources = {}
resources['azuread_user'] = json.loads(subprocess.check_output(['az', 'ad', 'user', 'list']))
resources['azuread_group'] = json.loads(subprocess.check_output(['az', 'ad', 'group', 'list']))
resources['azuread_application'] = json.loads(subprocess.check_output(['az', 'ad', 'app', 'list', '--all']))
resources['azuread_service_principal'] = json.loads(subprocess.check_output(['az', 'ad', 'sp', 'list', '--all']))

# THIS_DIR = os.path.dirname(os.path.abspath(__file__))

# j2_env = Environment(loader=FileSystemLoader(THIS_DIR))
j2_env = Environment(loader=FileSystemLoader('ad_templates/'))
# j2_env = Environment(loader=FileSystemLoader(THIS_DIR), trim_blocks=True)
templates = {}
for key in resources.keys():
    templates[key] = j2_env.get_template(f'{key}.tf.j2')

tr_table = str.maketrans('. /(@\'', '______')
tf = []
cmds = []
imports = []
os.makedirs('ad_tf', exist_ok=True)

def fix_val(val):
    name = val['displayName'].translate(tr_table).replace(')','')
    id = val['objectId']
    id_name = 'id_' + id.replace('-','_')
    newval = dict(val)
    newval['tf_name'] = id_name
    for k, v in val.items():
        if v == True:
            newval[k] = 'true'
        if v == False:
            newval[k] = 'false'
    return newval, id, id_name

#===============================================
# Users
#===============================================
print("Getting AD users")
with open('users.tf','w') as outfile, open('import.sh','w') as importfile:
    for val in resources['azuread_user']:
        newval, id, id_name = fix_val(val)
        outfile.write(templates['azuread_user'].render(**newval))
        outfile.write('\n')
        importfile.write(f'terraform import azuread_user.{id_name} "{id}"\n')
#
# #===============================================
# # Groups
# #===============================================
#

# az ad group member list -g ... --query '[*].objectId'
print("Getting AD groups")
with open('groups.tf','w') as outfile, open('import.sh','a') as importfile:
    for val in resources['azuread_group']:
        newval, id, id_name = fix_val(val)
        members = json.loads(
            subprocess.check_output(
                ['az', 'ad', 'group', 'member', 'list', '-g', id, '--query', '[*].objectId']
            )
        )
        owners = json.loads(
            subprocess.check_output(
                ['az', 'ad', 'group', 'owner', 'list', '-g', id, '--query', '[*].objectId']
            )
        )
        newval['members'] = members
        newval['owners'] = owners
        # newval['tf_name'] = id_name
        outfile.write(templates['azuread_group'].render(**newval))
        outfile.write('\n')
        importfile.write(f'terraform import azuread_group.{id_name} "{id}"\n')

# #===============================================
# # SPs
# #===============================================
#
print("Getting AD sps")
with open('service_principals.tf','w') as outfile, open('import.sh','a') as importfile:
    for val in resources['azuread_service_principal']:
        newval, id, id_name = fix_val(val)
        outfile.write(templates['azuread_service_principal'].render(**newval))
        outfile.write('\n')
        importfile.write(f'terraform import azuread_service_principal.{id_name} "{id}"\n')
#
#
# #===============================================
# # Apps
# #===============================================
print("Getting AD apps")
with open('apps.tf','w') as outfile, open('import.sh','a') as importfile:
    for val in resources['azuread_application']:
        newval, id, id_name = fix_val(val)
        outfile.write(templates['azuread_application'].render(**newval))
        outfile.write('\n')
        importfile.write(f'terraform import azuread_application.{id_name} "{id}"\n')
