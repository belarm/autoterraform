#!/usr/bin/env python3
import json
import subprocess
import os

def get_schema():
    if not os.path.isfile('full_schema.json'):
        proc = subprocess.Popen(["terraform","providers","schema","-json"],stdout=subprocess.PIPE)
        schema = json.load(proc.stdout)
        json.dump(schema, open('full_schema.json','w'), indent=3)
        return schema
    else:
        return json.load(open('full_schema.json'))

def parse_type(typedef, attr_name, lead, depth=0):
    if isinstance(typedef, str):
        return f"{{{{ {attr_name} }}}}"
    else:
        type, subtype = typedef
        lead += ' ' * 33
        if type in ['list', 'set']:
            output = f'[{{% for i_{depth} in {attr_name} %}}\n'
            output += lead + f'{{{{ i_{depth} }}}}{{% if not loop.last %}},{{% endif %}}\n'
            output += lead + "{% endfor %}]\n"
        elif type == 'map':
            output = f'{{ {{% for k, v in {attr_name}.items() %}}\n'
            output += lead + f'{{{{ k }}}} = {{{{ v }}}}{{% endfor %}}\n'
            output += lead + "}\n"
        else:
            output = "Can't parse " + type + " " + str(typedef)
        return output

def parse_block(decl, block, depth=0):
    output = "    " * depth + decl + " {\n"
    if 'attributes' in block:
        for attr_name, attr_def in block['attributes'].items():
            if 'computed' in attr_def and attr_def['computed'] == True:
                lead = '#   ' + '    ' * depth
            else:
                lead = '    ' * (depth + 1)
            rhs = parse_type(attr_def["type"], attr_name, lead, depth)
            output += f'{lead}{attr_name:30} = {rhs}\n'
    if 'block_types' in block:
        for block_name, block_def in block['block_types'].items():
            output += parse_block(block_name, block_def['block'], depth+1)
    output += "    " * depth + "}\n"
    return output

def make_templates():
    os.makedirs('templates',exist_ok=True)
    schema = get_schema()
    for provider_name, provider in schema['provider_schemas'].items():
        for name, resource in provider['resource_schemas'].items():
            print(f"Adding {name}")
            decl = f'resource "{name}" ' + '"{{ tf_name }}"'
            resource_template = parse_block(decl, resource['block'])
            with open(f'templates/resource_{name}.tf.j2','w') as templatefile:
                templatefile.write(resource_template)

if __name__ == '__main__':
    make_templates()
