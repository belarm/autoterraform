#!/usr/bin/env python3
import os
import json
from jinja2 import Environment, FileSystemLoader, meta
from collections import defaultdict
import operator
template_vars = {}
name_count = defaultdict(lambda: 0)

TDIR = os.path.abspath('./templates')
print(TDIR)
j2_env = Environment(loader=FileSystemLoader(TDIR))
for template in j2_env.list_templates():
    if template.endswith('.j2'):
        # print(template)
        tsource = j2_env.loader.get_source(j2_env, template)
        # print("----got source")
        pcontent = j2_env.parse(tsource)
        # print("-----parsed")
        vars = meta.find_undeclared_variables(pcontent)
        template_vars[template] = list(vars)
        for var in vars:
            name_count[var] += 1
        # unique_names.update(vars)
json.dump(template_vars, open('template_vars.json','w'), indent=3)
with open('name_counts.txt', 'w') as outfile:
    for k, v in sorted(name_count.items(), key=operator.itemgetter(1)):
        print(k)
        outfile.write(f"{k}: {v}\n")
# json.dump(name_count, open('name_count.json', 'w'), indent=3)
