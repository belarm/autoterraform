#!/usr/bin/env python3

import os
import json
import jmespath
from time import sleep
import string

resource_to_tfname = {
    "Microsoft.AAD/DomainServices": None,
    "Microsoft.Automation/automationAccounts": "azurerm_automation_account",
    "Microsoft.CertificateRegistration/certificateOrders": None,
    "Microsoft.ClassicStorage/storageAccounts": None,
    "Microsoft.Compute/disks": "azurerm_managed_disk",
    "Microsoft.Compute/restorePointCollections": None,
    "Microsoft.Compute/virtualMachines": "azurerm_virtual_machine",
    "Microsoft.Compute/virtualMachines/extensions": "azurerm_virtual_machine_extension",
    "Microsoft.DBforMySQL/servers": "azurerm_mysql_server",
    # "Microsoft.EventGrid/topics": "azurerm_eventgrid_topic",
    "microsoft.insights/actiongroups": "azurerm_monitor_action_group",
    # "microsoft.insights/alertrules": "azurerm_metric_alertrule",
    "microsoft.insights/autoscalesettings": "azurerm_autoscale_setting",
    # Sensitive value - come back?
    "microsoft.insights/components": "azurerm_application_insights",
    # "microsoft.insights/metricalerts": "azurerm_monitor_metric_alert",
    # "Microsoft.KeyVault/vaults": "azurerm_key_vault",
    "Microsoft.Logic/integrationAccounts": None,
    # "Microsoft.Logic/workflows": "azurerm_logic_app_workflow",
    "Microsoft.MarketplaceApps/classicDevServices": None,
    # "Microsoft.Network/dnszones": "azurestack_dns_zone",
    "Microsoft.Network/loadBalancers": "azurerm_lb",
    "Microsoft.Network/networkInterfaces": "azurerm_network_interface",
    "Microsoft.Network/networkSecurityGroups": "azurerm_network_security_group",
    "Microsoft.Network/networkWatchers": "azurerm_network_watcher",
    "Microsoft.Network/publicIPAddresses": "azurerm_public_ip",
    "Microsoft.Network/virtualNetworks": "azurerm_virtual_network",
    "Microsoft.OperationalInsights/workspaces": "azurerm_log_analytics_workspace",
    "Microsoft.OperationsManagement/solutions": "azurerm_log_analytics_solution",
    "Microsoft.Portal/dashboards": None,
    "Microsoft.RecoveryServices/vaults": "azurerm_recovery_services_vault",
    # Missing arguments
    # "Microsoft.Scheduler/jobcollections": "azurerm_scheduler_job_collection",
    "Microsoft.ServiceBus/namespaces": "azurerm_servicebus_namespace",
    "Microsoft.Sql/servers": "azurerm_sql_server",
    "Microsoft.Sql/servers/databases": "azurerm_sql_database",
    "Microsoft.SqlVirtualMachine/SqlVirtualMachines": None,
    # Sensitive value - come back?
    "Microsoft.Storage/storageAccounts": "azurerm_storage_account",
    "Microsoft.VisualStudio/account": None,
    "microsoft.visualstudio/account/project": None,
    "Microsoft.Web/certificates": None,
    # "Microsoft.Web/connections": None,
    "Microsoft.Web/serverFarms": None,
    "Microsoft.Web/sites": "azurerm_app_service",
    "Sendgrid.Email/accounts": None
}

state_string = """
# azurerm_virtual_network.PRODFREIGHTROVERvnet605:
resource "azurerm_virtual_network" "PRODFREIGHTROVERvnet605" {
    address_space       = [
        "10.0.3.0/24",
    ]
    dns_servers         = []
    id                  = "/subscriptions/1c32adc7-867b-493a-8992-6fa8acc914fa/resourceGroups/PROD-FREIGHTROVER/providers/Microsoft.Network/virtualNetworks/PRODFREIGHTROVERvnet605"
    location            = "centralus"
    name                = "PRODFREIGHTROVERvnet605"
    resource_group_name = "PROD-FREIGHTROVER"
    tags                = {}

    subnet {
        address_prefix = "10.0.3.0/24"
        id             = "/subscriptions/1c32adc7-867b-493a-8992-6fa8acc914fa/resourceGroups/PROD-FREIGHTROVER/providers/Microsoft.Network/virtualNetworks/PRODFREIGHTROVERvnet605/subnets/default"
        name           = "default"
    }
}
"""



try:
    # with open('terraform.tfstate','r') as infile:
    with open('state.json','r') as infile:
        state = json.load(infile)
        # existing_resources = jmespath.search("resources[].join('.', [type, name])", state)
        existing_resources = jmespath.search("resources[].instances[].attributes.id", state)
except:
    existing_resources = []
print(f"Found {len(existing_resources)} resources already in state")


import azure
from azure.common.client_factory import get_client_from_cli_profile

import re
ansi_escape = re.compile(r'\x1B[@-_][0-?]*[ -/]*[@-~]')

# from string import maketrans
tr_table = str.maketrans('. /(', '____')

def makenames(type, name):
    snake_name = name.translate(tr_table).replace(')','')
    type = resource_to_tfname[type]
    if snake_name[0] in string.digits:
        snake_name = 'a_' + snake_name
    return type, snake_name, f'{type}.{snake_name}'

# from azure.mgmt.compute import ComputeManagementClient
# client = get_client_from_cli_profile(ComputeManagementClient)
from azure.mgmt.resource import ResourceManagementClient
rc = get_client_from_cli_profile(ResourceManagementClient)
rc.resources.list()
resources = list(rc.resources.list())
to_tf = {}
count = 0
for resource in resources:
    if resource.type in resource_to_tfname and resource_to_tfname[resource.type] is not None:

        type, snake_name, full_name = makenames(resource.type, resource.name)
        tf_declr = f'resource "{type}" "{snake_name}" {{}}'
        if resource.id in existing_resources:
            print(f"Skipping {full_name} (id: {resource.id} already in state)")
            continue
        print(f"Will generate TF config for {full_name}")
        if resource.type not in to_tf:
            to_tf[resource.type] = []
        to_tf[resource.type].append(
            (tf_declr, resource)
        )
        count += 1
print(f"Planning to import {count} resources...")
sleep(10)
# Now we have a dictionary of lists of resources, sorted by type
to_run = []
to_run2 = []
to_delete = []
to_rename = []
for type, resources in to_tf.items():
    # Make two files - one to hold the skeleton definition to enable import,
    # the other to hold the finalized TF definition
    fname = f'{resource_to_tfname[type]}.tf.tmp'
    temp_fname = f'_temp_{resource_to_tfname[type]}.tf'
    with open(temp_fname,'w') as outfile:
        for declr, resource in resources:
            outfile.write(declr + '\n')
            type, snake_name, full_name = makenames(resource.type, resource.name)

            snake_name = resource.name.translate(tr_table)
            if snake_name[0] in string.digits:
                snake_name = 'a_' + snake_name
            to_run.append(f'terraform import {full_name} "{resource.id}"')
            to_run.append(f"terraform state show {full_name} |sed 's/\x1b\[[0-9;]*[a-zA-Z]//g' | grep -v '^    id ' >> {fname}")
    to_delete.append(temp_fname)
    to_rename.append(fname)
for cmd in to_run:
    print(cmd)
    os.system(cmd)
print("Deleteing temporary files...")
for file in to_delete:
    os.remove(file)
print("Moving new tf files into place...")
for file in to_rename:
    newname = file[:-4]
    os.rename(file, newname)
