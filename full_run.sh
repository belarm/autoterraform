#!/bin/bash
BASE_DIR=`dirname $0`
# echo $BASE_DIR
# export LOGLEVEL=INFO
export LOGLEVEL=DEBUG

if [ -f import.sh ]; then
    mv import.sh import.sh.bak
fi

${BASE_DIR}/test.py
terraform init
echo "importing resources..."
bash import.sh
