FROM python:3.6
RUN apt-get update && apt-get install -y \
    ca-certificates \
    curl \
    apt-transport-https \
    lsb-release \
    gnupg \
    libxml2-utils
RUN curl -sL https://packages.microsoft.com/keys/microsoft.asc | \
    gpg --dearmor | \
    tee /etc/apt/trusted.gpg.d/microsoft.asc.gpg > /dev/null

RUN echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ `lsb_release -cs` main" | \
    tee /etc/apt/sources.list.d/azure-cli.list
RUN apt-get update
RUN apt-get install azure-cli
RUN rm -rf /var/lib/apt/lists/*

RUN pip install \
    azure \
    jmespath \
    jinja2 \
    azure-cli-core

# RUN curl -sL https://aka.ms/InstallAzureCLIDeb | bash

ADD get_latest_terraform.sh /usr/local/bin/
RUN get_latest_terraform.sh
WORKDIR /source/tf
CMD ["/source/full_run.sh"]
#entrypoint ["/source/full_run.sh"]
