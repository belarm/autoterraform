#!/usr/bin/env python3

from azure.common.credentials import get_azure_cli_credentials
import json
creds, subid = get_azure_cli_credentials()
sess = creds.signed_session()
#res = sess.get(f"https://management.azure.com/subscriptions/{subid}/providers/Microsoft.Web/sites?api-version=2016-08-01")

default_providers_api_version="2019-09-01"

def get_provider_info(session, subid, api_version=default_providers_api_version):
    res = session.get(f"https://management.azure.com/subscriptions/{subid}/providers/?api-version={api_version}")
    providers = json.loads(res.text)['value']
    provider_info = {}
    for provider in providers:
        url = f"https://management.azure.com/{provider['id']}?api-version={api_version}"
        res = session.get(url)
        resourcetypes = json.loads(res.text)['resourceTypes']
        provider_info[provider['namespace']] = {}
        for rtype in resourcetypes:
            item = rtype
            if not item['locations']:
                # Item is not available in any location - skip it
                continue
            #print(provider['namespace'], item['resourceType'])
            #for apiv in item['apiVersions']:
            #    print(f'\t{apiv}')
            if item['apiVersions']:
                latest_api = sorted(item['apiVersions'])[-1] # Get greatest version
            else:
                # For some (exactly 2, as of writing) resource types, MS does not provide apiVersions.
                # So instead we try to retrieve a list of objects with a known-bad API version and extract
                # the valud ones from the error message
                print(f"Doing hacky shit for {provider['namespace'], item['resourceType']}")
                # Do some hacky shit
                bad_url = f"https://management.azure.com/{provider['id']}/{rtype['resourceType']}?api-version=none"
                mes = json.loads(session.get(bad_url).text)['error']['message']
                # print(mes)
                api_versions = sorted(mes[mes.find('versions are'):].split("'")[1].replace("'","").split(','))
                latest_api = api_versions[-1]
            item['latest_api'] = latest_api
            item['url'] = f"https://management.azure.com/{provider['id']}/{item['resourceType']}?api-version={latest_api}"
            provider_info[provider['namespace']][item['resourceType']] = item
    return provider_info
        

def get(session, url):
    res = session.get(url)
    text = json.loads(res.text)
    if res.status_code == 400 and text['error']['code'] == "InvalidApiVersionParameter":
        apiversion=mes[mes.find('The supported versions are'):].split("'")[1].replace("'","").split(',')[0]
    print(dir(res))


pinfo = get_provider_info(sess, subid)
with open('api_info.json','w') as outfile:
    json.dump(pinfo, outfile)
#res = sess.get(f"https://management.azure.com/subscriptions/{subid}/providers?api-version=2019-09-01")
#print(res.text)
