#!/usr/bin/env python3

import json
import csv
import os
from azure.common.credentials import get_azure_cli_credentials

with open('api_info.json','r') as infile:
    api_info = json.load(infile)
os.mkdir('items')
os.mkdir('errors')
creds, subid = get_azure_cli_credentials()
sess = creds.signed_session()
sess.headers['Content-Type'] = "application/json"
results = []
errors = []
for key, val in api_info.items():
    for k2, v2 in val.items():
        fname = f"{key}.{k2}.json".replace('/','.')
        res = sess.get(v2['url'])
        print(fname, res.status_code)

        if res.status_code == 200:
            if res.text:
                if len(res.text) > 13:
                    try:
                        with open("items/" + fname, 'w') as outfile:
                            json.dump(json.loads(res.text), outfile, indent=3)
                    except:
                        print("Could not JSON encode:")
                        print(res.text)
                        # pass
                else:
                    print("Got empty response")
        else:
            with open("errors/" + fname, 'w') as outfile:
                json.dump({
                    'code': res.status_code,
                    'text': res.text
                }, outfile, indent=3)
        results.append((fname, v2['url'], res.status_code))
        # print(fname, v2['url'])
        # print(res.status_code)
with open('az_results.csv','w') as outfile:
    w = csv.writer(outfile)
    w.writerows(results)
