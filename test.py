#!/usr/bin/env python3

# from autoterraform.terraformer import AzureADUsersTerraformer, AzureADServicePrincipalsTerraformer, AzureADApplicationsTerraformer, AzureADGroupsTerraformer
import autoterraform.terraformer
a = autoterraform.terraformer.AzureADUsersTerraformer()
a.render_templates()
b = autoterraform.terraformer.AzureADApplicationsTerraformer()
b.render_templates()
c = autoterraform.terraformer.AzureADServicePrincipalsTerraformer()
c.render_templates()
d = autoterraform.terraformer.AzureADGroupsTerraformer()
d.render_templates()
e = autoterraform.terraformer.AzureStorageAccount()
e.render_templates()
f = autoterraform.terraformer.AzureNetworkSecurityGroup()
f.render_templates()
